//
//  ViewController.swift
//  ARKitDemo
//
//  Created by Jaime Vega on 7/17/17.
//  Copyright © 2017 Bottle Rocket Studios. All rights reserved.
//

import UIKit
import ARKit

class ViewController: UIViewController {
    
    @IBOutlet var arSceneView: ARSCNView!
    
    var spaceCraft: SCNNode!
    var initialPosition: SCNVector3!
    
    @IBAction func decrementRotationTapped(_ sender: Any) {
        let decrementRotation = SCNAction.rotateBy(x: 0, y: -6.28, z: 0, duration: 1)
        spaceCraft.runAction(decrementRotation)
    }
    
    @IBAction func incrementRotationTapped(_ sender: Any) {
        let incrementRotation = SCNAction.rotateBy(x: 0, y: 6.28, z: 0, duration: 1)
        spaceCraft.runAction(incrementRotation)
    }
    
    @IBAction func position1Tapped(_ sender: Any) {
        SCNTransaction.begin()
        SCNTransaction.animationDuration = 2.0
        spaceCraft.position = initialPosition
        SCNTransaction.commit()
        SCNTransaction.flush()
    }
    
    @IBAction func position2Tapped(_ sender: Any) {
        SCNTransaction.begin()
        SCNTransaction.animationDuration = 2.0
        spaceCraft.position = SCNVector3(x: 1.5, y:2.1, z: -9)
        SCNTransaction.commit()
        SCNTransaction.flush()
    }
    
    @IBAction func position3Tapped(_ sender: Any) {
        SCNTransaction.begin()
        SCNTransaction.animationDuration = 2.0
        spaceCraft.position = SCNVector3(x: -1.5, y:2.1, z: -9)
        SCNTransaction.commit()
        SCNTransaction.flush()
    }
    
    @IBAction func toggleDebugOptions(_ sender: UISwitch) {
        if sender.isOn {
            arSceneView.debugOptions = [.showLightExtents, .showLightInfluences, .showWireframe, .showBoundingBoxes]
        } else {
            arSceneView.debugOptions = []
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let scene = SCNScene(named: "art.scnassets/TR3.dae")!
        arSceneView.scene = scene
        
        spaceCraft = scene.rootNode.childNodes[0]
        spaceCraft.position = SCNVector3(x: 0, y: 0.3, z: -3)
        spaceCraft.scale = SCNVector3(x: 0.5, y: 0.5, z: 0.5)
        
        initialPosition = spaceCraft.position
        
        let omniLight = SCNLight()
        omniLight.type = .omni
        
        let omniLightNode = SCNNode()
        omniLightNode.light = omniLight
        omniLightNode.position = SCNVector3(x: 0.3, y: 3, z: 0)
        
        scene.rootNode.addChildNode(omniLightNode)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        arSceneView.session.pause()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let configuration = ARWorldTrackingSessionConfiguration()
        arSceneView.session.run(configuration)
    }
}

